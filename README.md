# vSphere VM Creation terraform module

This module creates one or multiple Linux or Windows Virtual Machine in vSphere.

This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

## Usage Examples

The [examples](examples/) directory contains examples of use. A basic usage
example is available in [examples/root-example](examples/root-example/).

## Resources
- [Terraform-VMWare-Modules](https://github.com/Terraform-VMWare-Modules)

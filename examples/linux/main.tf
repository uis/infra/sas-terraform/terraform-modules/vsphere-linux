// Example of provisoning two Linux VMs
module "example-server-linuxvm" {
  source                 = "git@gitlab.developers.cam.ac.uk:uis/infra/sas-terraform/terraform-modules/vsphere-linux.git"
  dc                     = "UIS"
  vmrp                   = "wcdc-terraform-test"
  vmfolder               = "Vm automation/Test"
  ds_cluster             = "" # There's no datastore cluster
  vmtemp                 = "RHEL7Template"
  instances              = 2
  cpu_number             = 2
  ram_size               = 2096
  cpu_hot_add_enabled    = "true"
  cpu_hot_remove_enabled = "true"
  memory_hot_add_enabled = "true"
  vmname                 = "TerraformVM"
  vmdomain               = "internal.admin.cam.ac.uk"
  network_cards          = ["DEV_VLAN"]
  ipv4submask            = ["24"]
  ipv4 = {
    "DEV_VLAN"  = ["firs-vm-ip", "second-vm-ip"] // an IP for each VM instance
  }
  disk_label                = ["tpl-disk-1"]
  data_disk_label           = ["label1", "label2"]
  scsi_type                 = "pvscsi" # "lsilogic" 
  scsi_controller           = 0
  data_disk_scsi_controller = [0, 1]
  disk_datastore            = "WC_UIS_SC_VM_vol1"
  data_disk_datastore       = ["WC_UIS_SC_VM_vol1", "WC_UIS_SC_VM_vol1"]
  data_disk_size_gb         = [10, 15] // Additional Disks to be used
  thin_provisioned          = ["true", "false"]
  vmdns                     = ["10.0.64.1", "10.1.2.2", "10.1.2.3"]
  vmgateway                 = "10.0.32.254"
}

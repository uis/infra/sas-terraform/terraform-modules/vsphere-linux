# Simple example
This is an example of how creating two Linux VMs on vSphere.

```hcl-terraform
$ terraform init
$ terraform plan -var-file=./vm.tfvars -var-file=./secrets.tfvars
```
